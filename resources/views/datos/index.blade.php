@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h1>DATOS PERSONALES</h1><br>
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO PATERNO</th>
                        <th>APELLIDO MATERNO</th>
                        <th>FECHA NACIMIENTO</th>
                        <th>OPCIONES</th>
                    </tr>
                    @foreach($dato as $da)
                        <tr>
                            <td>{{$da->id}}</td>
                            <td>{{$da->nombre}}</td>
                            <td>{{$da->apellidopaterno}}</td>
                            <td>{{$da->apellidomaterno}}</td>
                            <td>{{$da->fechanacimiento}}</td>
                            <td>
                                <a href="{{url('/datos/'.$da->id.'/edit')}}" class="btn btn-secondary">Editar</a>
                                @include('datos.delete',['$da' => $da])
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <form action="{{route('datos.create')}}" method="GET">
                <input type="submit" value="Nuevo" class="btn btn-primary text-white">
            </form>
        </div>
    </div>
@endsection
