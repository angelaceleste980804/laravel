@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Datos Personales</h1>

                <form action="{{route('datos.update',$dato->id)}}" method="POST">
                    {{method_field('PATCH')}}
                    @csrf
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" class="form-control" value="{{$dato->nombre}}">
                    <label for="apellidopaterno">Apellido Paterno</label>
                    <input type="text" name="apellidopaterno" class="form-control" value="{{$dato->apellidopaterno}}">
                    <label for="apellidomaterno">Apellido Materno</label>
                    <input type="text" name="apellidomaterno" class="form-control" value="{{$dato->apellidomaterno}}">
                    <label for="fechanacimiento">Fecha Nacimiento</label>
                    <input type="date" name="fechanacimiento" class="form-control" value="{{$dato->fechanacimiento}}">
                    <br>
                    <input type="submit" value="Guardar" class="btn btn-primary">
                </form>

            </div>
        </div>
    </div>
@endsection
